Access the website at [90mininone.me](90mininone.me)

ABOUT

Hey! We're the developers behind 90 min in one. Get to know a little more about us below! Also, explore our API and discover the development tools that we used.
OUR MISSION
Soccer players have one goal. Literally. Here at 90 min in one, we do too!
With more than 4 billion fans worldwide, soccer has skyrocket to become the most popular sport. Learning about soccer has never been easier with 90 min in one. We seek to provide an engaging site that feeds the curiosity behind soccer statistics through the delivery of simple, informative data sets about soccer leagues, teams, and players in a clean and understandable way.
More precisely, we’ve gathered data on 1300+ leagues, 50 of the most popular teams, and their players. Like a certain league? Find out what players have played under that league. Curious about a certain player? Explore the team they play for. We’ve grouped data such that curiosity can flow seamlessly from category to category and provide a quality learning experience.
We hope you get a kick out of it! Get it?


VIDEO PRESENTATION

https://youtu.be/56EMnyt-Emo

API Documentation
https://documenter.getpostman.com/view/10502654/SzS8uRNf?version=latest 