import React, { Component } from 'react';
import jsonData from "../../assets/teams.json";
import {Table} from 'reactstrap';
import {Button} from 'react-bootstrap';
import history from '../../history';
class ManchesterUnited extends Component {
    state = {
        teams: {}
    }
    componentDidMount() {
            this.setState({teams:jsonData.api.teams[0]}, 
                () => {console.log(this.state.teams);}) 
    }    
    
    abc = () => (
                <tr>
                <td>{this.state.teams['name'] }</td>
                <td>{this.state.teams['code']}</td>
                <td><img src = {this.state.teams['logo']} width="30"></img></td>
                </tr>
    );

    get_tables = () => {

        let attrs =  Object.keys(this.state.teams).filter(
            key => ['name', 'code', 'logo', 'team_id', 'is_national'].indexOf(key) == -1
        );
        
        return (
            <Table>
                <thead>
                    <tr>
                        {attrs.map(attr => <th>{attr}</th>)}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {attrs.map(attr => <td>{this.state.teams[attr]}</td>)}
                    </tr>
                </tbody>
            </Table>
        );
    };

    render() {
        return (
            <React.Fragment>
            <div style={{ display: 'flex', justifyContent: 'center', padding: 30 }}>
                <div><h2>Manchester United</h2></div>
            </div>
            <div>
                <Table>
                    <thead>
                        <tr>
                            <th>Team Name</th>
                            <th>Code</th>
                            <th>Logo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.abc()}
                    </tbody>
                </Table>
                {this.get_tables()}
            </div>
            <Button onClick={() => history.push('/Players/')}>Players in Manchester United</Button>
            </React.Fragment>
        );
    }
}

export default ManchesterUnited;