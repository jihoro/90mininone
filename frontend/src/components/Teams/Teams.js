import React, { Component } from 'react';
import { Container, Row, Button } from 'react-bootstrap';
import ReactLoading from 'react-loading';
import Pagination from "../Pagination/Pagination";
import DisplayPageStats from "../Pagination/DisplayPageStats";
import Search from "../Search";
import Loading from "../Loading/Loading";
import FadeIn from "react-fade-in";
import TeamCard from "./TeamCard"
import "./Teams.css"
import baseUrl from '../../assets/baseUrl';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';


class Teams extends Component {

    constructor(props) {
        super(props)
        this.state = {
            all_teams: [],
            current_teams: [],
            currentPage: null,
            totalPages: null,
            offset: 0,
            pageLimit: 0,
            filterText: '',
            filterVals: {},
            id: 0,
            done: undefined,
            loading: undefined,
            sorted: false,
            openFilter: false,
            openSort: false
        }
    }

    componentDidMount() {
        setTimeout(() => {
            fetch(baseUrl + "/Teams")
                .then(response => response.json())
                .then(data => {
                    this.setState({ loading: true });
                    setTimeout(() => {
                        this.setState({ done: true, all_teams: data.teams });
                    }, 2000);
                });
        }, 1200);
    }

    filterUpdate(value) {
        this.setState({
            filterText: value,

            // id used to make new paginatgion bar and pages on search instead of trying to update it.
            // changing some prop (this one) to pagination component every time should force this action
            // has to do with react component state based on prop. so lifecycle events require this
            id: !this.state.id
        })
    }

    filterValUpdate(key, value) {
        this.state.filterVals[key] = value;
        this.setState({
            id: !this.state.id
        })
    }

    //helper function to check whether searchable team attirbutes match search query
    // EDIT this when adding/removing attributes we want to match search to
    searchTeamAttributes(team) {
        const search = this.state.filterText.trim().toLowerCase();
        if (search.length === 0) {
            return 1;
        }
        const keywords = search.trim().split(' ');

        const name = team.name.toLowerCase();
        const venue = team.venue_name.toLowerCase();
        const year = String(team.founded).toLowerCase();
        const country = team.country.toLowerCase();

        let score = 0;

        // loop through search keywords and include if keyword present in attributes
        for (let i = 0; i < keywords.length; i++) {
            let word = keywords[i];

            let nameMatch = name.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let yearMatch = year.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let venueMatch = venue.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let countryMatch = country.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;

            score += nameMatch + yearMatch + venueMatch + countryMatch;
        }
        return score;
    }

    findFilteredTeam(team) {
        let res = true;
        for(const [key, val] of Object.entries(this.state.filterVals)) {
            if (val.length > 0) {
                let cur_res = false;
                if (val.indexOf(team[key]) > -1) {
                    cur_res = true;
                }
                res = res && cur_res;
            }
        }
        return res;
    }

    //helper function that returns filter data
    filterFunction(all_teams) {
        return all_teams
            .filter(team => this.findFilteredTeam(team))
            .sort((a, b) => this.sortComparator(a, b))
            .map(team => [this.searchTeamAttributes(team), team])
            .filter(teamTuple => {
                //remove teams from all fetched leauges whose names don't match the filtered keyword
                return teamTuple[0] > 0;
            })
            .sort((a, b) => b[0] - a[0])
            .map(teamTuple => {
                return (
                    <TeamCard
                        team={teamTuple[1]} 
                        searchQuery = {this.state.filterText.toLowerCase()}
                    />
                )
            })
    }

    sortComparator = (a, b) => {
        if (this.state.sorted){
            return a.name.trim().localeCompare(b.name.trim());
        }
        // return b.name.trim().localeCompare(a.name.trim());
    }

    // callback function for page change. passed to Pagination component
    onPageChanged = data => {
        const { all_teams } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        // -1 accounts for zero-based indexing. We're fetching the next chunk of teams to display
        const offset = (currentPage - 1) * pageLimit;
        const current_teams = this.filterFunction(all_teams).slice(offset, offset + pageLimit);

        // set current page to new 'page' of teams. (essentially: Just changing what chunks of teams get displayed) 
        this.setState({ currentPage, current_teams, totalPages, offset, pageLimit });
    };

    toggleFilter = () => {
        let cur_filter = this.state.openFilter;
        this.setState({openFilter: !cur_filter, openSort: false});
    }

    toggleSort = () => {
        let cur_sort = this.state.openSort;
        this.setState({openSort: !cur_sort, openFilter: false});
    }

    handleCountryFilterChange = (selectedOption) => {
        if (selectedOption === null) {
            selectedOption = [];
        }
        selectedOption = selectedOption.map(option => option.value);
        this.filterValUpdate('country', selectedOption);
    }

    displayDropdown = () => {
        const countries = [...new Set(this.state.all_teams.map(team => team.country))]
                            .sort()
                            .map(country => ({ value: country, label: country }));

        return (
            <React.Fragment>
                Country:
                <Select
                    options={countries}
                    isMulti
                    onChange={this.handleCountryFilterChange}
                    components={makeAnimated()}
                />
            </React.Fragment>
        );
    }

    handleToggle = () => {
        this.setState({sorted: !this.state.sorted, id: !this.state.id})
    }

    displaySort = () => {
        return (
            <div className="sort">
            <div className="sortText">Sort Alphabetically by Name</div>
              <input
                checked={this.state.sorted}
                onChange={this.handleToggle}
                className="react-switch-checkbox"
                id={`react-switch-new`}
                type="checkbox"
              />
              <label
                style={{ background: this.state.sorted && '#06D6A0' }}
                className="react-switch-label"
                htmlFor={`react-switch-new`}
              >
                <span className={`react-switch-button`} />
              </label>
            </div>
        )
    }

    render() {
        const {
            all_teams,
            current_teams,
            currentPage,
            totalPages,
            offset
        } = this.state;

        const modelName = "Teams";
        let displayData = { modelName };
        let searchList = []

        if (this.state.done) {
            const total_teams = all_teams.length;
            if (total_teams === 0) return null;
            let numDisplayed = offset + 1;

            // creates "list" of teams filtered by search
            searchList = this.filterFunction(all_teams)


            // group data to send together to component DisplayPageStats
            displayData = {
                numDisplayed,
                offset,
                searchList,
                totalPages,
                currentPage,
                modelName,
                current_instances: current_teams
            };
        }

        return (
            <Container>

                {/* page header */}
                <Row className="space"></Row>
                <br></br>
                <br></br>
                <br></br>
                <Row className="about-row"><h1>TEAMS</h1></Row>
                <br></br>


                {!this.state.done ? (
                    <div><Row>
                        <Loading loading={this.state.loading} />
                    </Row></div>
                ) : (
                    <FadeIn>
                        <div>
                            {/* page stats */}
                            <DisplayPageStats data={displayData} />
                            <br></br>
                            <br></br>

                            {/* {sort} */}
                            

                            {/* search bar */}
                            <div className="filterTools">
                                <Search
                                    filterUpdate={this.filterUpdate.bind(this)}
                                    filterText={this.state.filterText}
                                    model={modelName}
                                    placeHolder="Enter a team's name, year, country, or venue name..."
                                />
                                <Button onClick={this.toggleFilter}> Filter </Button>
                                <Button onClick={this.toggleSort}> Sort </Button>
                                <br />
                                {
                                    this.state.openFilter ? this.displayDropdown() : this.state.openSort ? this.displaySort() : null
                                }
                            </div>
                            
                            {/* model cards */}
                            <Row className="about text-center">
                                {current_teams}
                            </Row>
                           
                            {/* pagination bar */}
                            <Row className="d-flex flex-row py-4 justify-content-center align-items-center">
                                <Pagination
                                    totalRecords={searchList.length}
                                    key={this.state.id}
                                    pageLimit={12}
                                    pageNeighbours={1}
                                    onPageChanged={this.onPageChanged}
                                />
                            </Row>
                            <br></br>
                            <br></br>
                        </div>
                    </FadeIn>
                )}
            </Container>
        );
    }
}

export default Teams;
