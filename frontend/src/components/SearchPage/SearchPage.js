import React, { Component } from 'react';
import { Container, Row, Button, ToggleButtonGroup, ToggleButton} from 'react-bootstrap';

import PlayerCard from "../Players/PlayerCard.js";
import LeagueCard from "../Leagues/LeagueCard.js";
import TeamCard from "../Teams/TeamCard.js";

import Pagination from "../Pagination/Pagination";
import DisplayPageStats from "../Pagination/DisplayPageStats";
import Search from "../Search";
import Loading from "../Loading/Loading";
import FadeIn from "react-fade-in";

import "../Players/Players.css";
import "./Search.css";
import baseUrl from '../../assets/baseUrl.js';

import Lottie from "react-lottie";
import * as ballLoading from "./bouncingLoad.json";




class SearchPage extends Component {


    constructor(props) {
        super(props)
        this.state = {
            all_players: [],
            all_leagues: [],
            all_teams:[],
            current_players: [],

            currentPage: null,
            totalPages: null,
            offset: 0,
            pageLimit: 0,

            filterText: '',
            filterVals: {},
            id: 0,

            done: false,
            done2: false,
            done3: false,
            loading: undefined,

            searchList: [],
            display: "",

            sorted: true
        }
    }

    componentDidMount() {
        setTimeout(() => {
            fetch(baseUrl + "/Players")
                .then(response => response.json())
                .then(data => {
                    this.setState({ loading: true });
                    setTimeout(() => {
                        this.setState({ done: true, all_players: data.players });
                    }, 2000);
                }); 

            fetch(baseUrl + "/Teams")
            .then(response => response.json())
            .then(data => {
                this.setState({ loading: true });
                setTimeout(() => {
                    this.setState({ done2: true, all_teams: data.teams });
                }, 2000);
            });

            fetch(baseUrl + "/Leagues")
                .then(response => response.json())
                .then(data => {
                    this.setState({ loading: true });
                    setTimeout(() => {
                        this.setState({ done3: true, all_leagues: data.leagues});
                    }, 2000);
            }); 
            
            // this.setState({id: !this.state.id});
        }, 2000);

    }

    filterUpdate(value) {
        this.setState({
            filterText: value,

            // id used to make new paginatgion bar and pages on search instead of trying to update it.
            // changing some prop (this one) to pagination component every time should force this action
            // has to do with react component state based on prop. so lifecycle events require this
            id: !this.state.id
        })
    }

    //helper function to check whether searchable player attirbutes match search query
    searchPlayerAttributes(player) {
        const search = this.state.filterText.trim().toLowerCase();
        if (search.length === 0) {
            return 1;
        }
        const keywords = search.trim().split(' ');

        const name = player.player_name.toLowerCase();
        const team = player.team.name.toLowerCase();
        const position = player.position.toLowerCase();
        const league = player.league.toLowerCase();
        const age = String(player.age).toLowerCase();

        let score = 0;

        // loop through search keywords and include if keyword present in attributes
        for (let i = 0; i < keywords.length; i++) {
            let word = keywords[i];

            let nameMatch = name.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let teamMatch = team.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let posMatch = position.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let leagueMatch = league.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let ageMatch = age.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;

            score += nameMatch + teamMatch + posMatch + leagueMatch + ageMatch;
        }
        return score;
    }

     //helper function to check whether searchable league attirbutes match search query
    searchLeagueAttributes(league) {
        const search = this.state.filterText.toLowerCase();
        if (search.length === 0) {
            return 1;
        }
        const keywords = search.trim().split(' ');

        const name = league.name.toLowerCase();
        const cc = league.country_code.toLowerCase();
        const country = league.country.toLowerCase();

        let score = 0;

        // loop through search keywords and include if keyword present in attributes
        for (let i = 0; i < keywords.length; i++) {
            let word = keywords[i];

            let nameMatch = name.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let ccMatch = cc.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let countryMatch = country.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;

            score += nameMatch + ccMatch + countryMatch;
        }
        return score;
    }

     //helper function to check whether searchable team attirbutes match search query
    // EDIT this when adding/removing attributes we want to match search to
    searchTeamAttributes(team) {
        const search = this.state.filterText.trim().toLowerCase();
        if (search.length === 0) {
            return 1;
        }
        const keywords = search.trim().split(' ');

        const name = team.name.toLowerCase();
        const venue = team.venue_name.toLowerCase();
        const year = String(team.founded).toLowerCase();
        const country = team.country.toLowerCase();

        let score = 0;

        // loop through search keywords and include if keyword present in attributes
        for (let i = 0; i < keywords.length; i++) {
            let word = keywords[i];

            let nameMatch = name.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let yearMatch = year.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let venueMatch = venue.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let countryMatch = country.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;

            score += nameMatch + yearMatch + venueMatch + countryMatch;
        }
        return score;
    }

    //helper function that returns filter data
    filterFunction(all_players) {
        return all_players
        .map(player => [this.searchPlayerAttributes(player), player])
        .filter(playerTuple => {
            //remove teams from all fetched leauges whose names don't match the filtered keyword
            return playerTuple[0] > 0;
        })
        .sort((a, b) => b[0] - a[0])
        .map(playerTuple => {
            return (
                <PlayerCard 
                    player={playerTuple[1]} 
                    searchQuery = {this.state.filterText}
                />
            )
        })
    }

     //helper function that returns filter data
     filterLeagues(all_leagues) {
        const search = this.state.filterText.toLowerCase();
        return all_leagues
            .map(league => [this.searchLeagueAttributes(league), league])
            .filter(leagueTuple => {
                //remove leagues from all fetched leauges whose names don't match the filtered keyword
                return leagueTuple[0] > 0;
            })
            .sort((a, b) => b[0] - a[0])
            .map(leagueTuple => {
                return (
                    <LeagueCard 
                        league={leagueTuple[1]} 
                        searchQuery = {this.state.filterText.toLowerCase()}
                    />
                )
            })
    }

     //helper function that returns filter data
     filterTeams(all_teams) {
        return all_teams
            .map(team => [this.searchTeamAttributes(team), team])
            .filter(teamTuple => {
                //remove teams from all fetched leauges whose names don't match the filtered keyword
                return teamTuple[0] > 0;
            })
            .sort((a, b) => b[0] - a[0])
            .map(teamTuple => {
                return (
                    <TeamCard
                        team={teamTuple[1]} 
                        searchQuery = {this.state.filterText.toLowerCase()}
                    />
                )
            })
    }


    displayLeagues(all_leagues){
        // console.log("l");
        this.setState({display: "Leagues", id: !this.state.id});
        // console.log(this.state.display);
    }

    displayTeams(all_teams){
        // console.log("t");
        this.setState({display: "Teams", id: !this.state.id});
        // console.log(this.state.display);
    }


    displayPlayers(all_players){
        // console.log("p");
        this.setState({display: "Players", id: !this.state.id});
        // console.log(this.state.display);
    }

    sortComparator = (a, b) => {
        if (this.state.sorted){
            return a.player_name.trim().localeCompare(b.player_name.trim());
        }
        return b.player_name.trim().localeCompare(a.player_name.trim());
    }

    // callback function for page change. passed to Pagination component
    onPageChanged = data => {
        const { all_players, all_leagues, all_teams, display} = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        // -1 accounts for zero-based indexing. We're fetching the next chunk of players to display
        const offset = (currentPage - 1) * pageLimit;
            
        console.log("pageChange " +  this.state.display);

        var current_players = [];
        if(display === "Players")
            current_players = this.filterFunction(all_players).slice(offset, offset + pageLimit);
        else if(display === "Leagues")
            current_players = this.filterLeagues(all_leagues).slice(offset, offset + pageLimit);
        else if (display === "Teams")
            current_players = this.filterTeams(all_teams).slice(offset, offset + pageLimit);

        // set current page to new 'page' of players. (essentially: Just changing what chunks of players get displayed) 
        this.setState({ currentPage, current_players, totalPages, offset, pageLimit });
    };


    render() {
        const {
            all_players,
            current_players,

            all_leagues,

            all_teams,

            currentPage,
            totalPages,
            offset
        } = this.state;

        const modelName = "through all Leagues, Teams, and Players";
        let displayData = {};
        let searchList = []

        if (this.state.done && this.state.done2 && this.state.done3) {
            console.log("hi");
            const total_players = all_players.length;
            if (total_players === 0) return null;
            let numDisplayed = offset + 1;

            // creates "list" of players filtered by search
            if(this.state.display === "Players")
                searchList = this.filterFunction(all_players);
            else if(this.state.display === "Leagues")
                searchList = this.filterLeagues(all_leagues);
            else if (this.state.display === "Teams")
                searchList = this.filterTeams(all_teams);

            // group data to send together to component DisplayPageStats
            displayData = {
                numDisplayed,
                offset,
                searchList,
                totalPages,
                currentPage,
                modelName: this.state.display,
                current_instances: current_players
            };
        }

        console.log(searchList);

        return (
            <Container>

                {/* page header */}
                <Row className="space"></Row>
                <br></br>
                <br></br>
                <br></br>
                <Row className="about-row"><h1>SEARCH</h1></Row>
                <br></br>

                {((!this.state.done) && (!this.state.done2) && (!this.state.done3))? (
                    <div><Row>
                        <Loading loading={this.state.loading} />
                    </Row></div>
                ) : (
                    <FadeIn>
                        <div>
                            {/* page stats */}
                            <DisplayPageStats data={displayData} />
                            <br></br>
                            <br></br>

                            {/* search bar */}
                            <div className="filterTools">
                                <Search
                                    filterUpdate={this.filterUpdate.bind(this)}
                                    filterText={this.state.filterText}
                                    model={modelName}
                                    placeHolder="Enter a player, team, league, year, country, etc..."
                                />
                                <br></br>
                                <Row className="dflex justify-content-center">
                                    <ToggleButtonGroup type="radio" name="options">
                                        <ToggleButton onClick={() => this.displayLeagues(this.state.all_leagues)} value={1}>Leagues</ToggleButton>
                                        <ToggleButton onClick={() => this.displayTeams(this.state.all_teams)} value={2}>Teams</ToggleButton>
                                        <ToggleButton onClick={() => this.displayPlayers(this.state.all_players)} value={3}>Players</ToggleButton>
                                    </ToggleButtonGroup>
                                </Row>
                                <br></br>
                            </div>

                            {/* model cards */}
                            <Row className="about text-center">
                                {current_players}
                            </Row>   
                                  
                            <Row className="d-flex flex-row py-4 justify-content-center align-items-center">
                                <Pagination
                                    totalRecords={searchList.length}
                                    key={this.state.id}
                                    pageLimit={12}
                                    pageNeighbours={1}
                                    onPageChanged={this.onPageChanged}
                                />
                            </Row>
                            <br></br>
                            <br></br>
                        </div>
                    </FadeIn>
                )}
            </Container>
        );
    }
}

export default SearchPage;
