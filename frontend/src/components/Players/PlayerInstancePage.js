import React, { useState, useEffect } from 'react';
import { Image, Container, Row, Col, ListGroup, Button } from 'react-bootstrap';
import baseUrl from '../../assets/baseUrl';
import { Link } from "react-router-dom";
import "./Players.css"


const PlayerInstancePage = (props) => {
    const player_id = props.location.state.player_id;
    // console.log(player_id)

    const [player, setPlayer] = useState({});
    const [leagues, setLeagues] = useState(null);

    async function fetchPlayer() {
        const res = await fetch(baseUrl + "/Players/" + player_id);
        res
            .json()
            .then(res => setPlayer(res));
    }

    useEffect(() => {
        fetchPlayer();
    }, []);

    const { 
        player_name, 
        league, 
        position,
        height,
        weight,
        nationality,
        birth_date,
        birth_place,
        firstname = " ", 
        lastname = " ",
        team
    } = player;

    const {
        name = " ",
        logo,
        team_id = 0
    } = team || {};

    //fetches league based on team_id and sets state "league"
    async function fetchLeagues(tId) {
        console.log(tId);
        const res = await fetch(baseUrl + "/TeamAndLeague?team_id=" + tId);
        res
            .json()
            .then(res => setLeagues(res));
    }

    //use effect that gets called when team_id changes from 0 to x. Calls fetchLeagues()
    useEffect(() => {
        fetchLeagues(team_id);
        console.log(leagues);
    }, [team_id]);

    return (
        <div className="background">
        <Container className="instanceCon">
            <Row className="space"></Row>
            <br></br>
            <br></br>
            <br></br>
            <Row className="d-flex ">
                <Col className="playerStatsCard">
                    <Row className="d-flex flex-column align-items-center">
                        <h1 className="text-center">{firstname.toUpperCase()}</h1> 
                        <h1 className="text-center">{lastname.toUpperCase()}</h1>
                    </Row>
                    <br></br>
                    <Row>
                        <ListGroup variant="flush">
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Nationality: &#160;</div>
                                <div >{nationality}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Position: &#160;</div>
                                <div >{position}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Height: &#160;</div>
                                <div >{height}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Weight: &#160;</div>
                                <div >{weight}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Birthday: &#160;</div>
                                <div >{birth_date}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Birth Place: &#160;</div>
                                <div >{birth_place}</div>
                            </ListGroup.Item>
                        </ListGroup>
                    </Row>
                </Col>

                <Col className="teamAndLeagueCards">
                    <Row className="teamCardLink">
                        <Col className="text-center" xs={6}>
                            <h3>TEAM</h3>
                            <Link className="linktext" to={{
                                    pathname: '/Teams/' + name.replace(' ', ''),
                                    state: {
                                        team_id: team_id
                                    }
                                }}>
                                <h3 style={{fontSize: '1.5rem'}}>{name}</h3>
                            </Link>
                        </Col>
                        <Col xs={6}>
                            <Image className="teamLogoImage" src={logo} rounded fluid/>
                        </Col>
                    </Row>
                    <br></br>
                    <br></br>
                    <Row>
                        <div className="leagueCardLink flex-column">
                            <h3>LEAGUES</h3>
                            <br></br>
                            <div>
                                {leagues != null && leagues.length != 0 ? (
                                    <div>
                                        {leagues
                                            .filter(x => {return x.league != 'None'})
                                            .map(league => (
                                            <Link className="linktext" to={{
                                                pathname: '/Leagues/' + league.league.replace(' ', ''),
                                                state: {
                                                    league_id: league.league_id
                                                }
                                            }}>
                                                <h3>{league.league}</h3>
                                            </Link>
                                            ))
                                        }
                                    </div>
                                ) : ''}
                            </div>
                        </div>
                    </Row>
                </Col>
            </Row>
            <br></br>
            <br></br>
        </Container>
        </div>
    );
}

export default PlayerInstancePage;
  
