import React, { Component } from 'react';
import { Container, Row, Button } from 'react-bootstrap';
import PlayerCard from "./PlayerCard.js"
import Pagination from "../Pagination/Pagination";
import DisplayPageStats from "../Pagination/DisplayPageStats";
import Search from "../Search";
import Loading from "../Loading/Loading";
import FadeIn from "react-fade-in";
import "./Players.css";
import baseUrl from '../../assets/baseUrl.js';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';


class Players extends Component {


    constructor(props) {
        super(props)
        this.state = {
            all_players: [],
            current_players: [],
            currentPage: null,
            totalPages: null,
            offset: 0,
            pageLimit: 0,
            filterText: '',
            filterVals: {},
            id: 0,
            done: undefined,
            loading: undefined,
            sorted: false,
            openFilter: false,
            openSort: false
        }
    }

    componentDidMount() {
        setTimeout(() => {
            fetch(baseUrl + "/Players")
                .then(response => response.json())
                .then(data => {
                    this.setState({ loading: true });
                    setTimeout(() => {
                        this.setState({ done: true, all_players: data.players });
                    }, 2000);
                });
        }, 1200);
    }

    filterUpdate(value) {
        this.setState({
            filterText: value,

            // id used to make new paginatgion bar and pages on search instead of trying to update it.
            // changing some prop (this one) to pagination component every time should force this action
            // has to do with react component state based on prop. so lifecycle events require this
            id: !this.state.id
        })
    }

    filterValUpdate(key, value) {
        this.state.filterVals[key] = value;
        this.setState({
            id: !this.state.id
        })
    }

    //helper function to check whether searchable league attirbutes match search query
    searchPlayerAttributes(player) {
        const search = this.state.filterText.trim().toLowerCase();
        if (search.length === 0) {
            return 1;
        }
        const keywords = search.trim().split(' ');

        const name = player.player_name.toLowerCase();
        const team = player.team.name.toLowerCase();
        const position = player.position.toLowerCase();
        const league = player.league.toLowerCase();
        const age = String(player.age).toLowerCase();

        let score = 0;

        // loop through search keywords and include if keyword present in attributes
        for (let i = 0; i < keywords.length; i++) {
            let word = keywords[i];

            let nameMatch = name.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let teamMatch = team.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let posMatch = position.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let leagueMatch = league.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let ageMatch = age.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;

            score += nameMatch + teamMatch + posMatch + leagueMatch + ageMatch;
        }

        return score;
    }

    findFilteredPlayer(player) {
        let res = true;
        for(const [key, val] of Object.entries(this.state.filterVals)) {
            if (val.length > 0) {
                let cur_res = false;
                if (key === 'team' ? val.indexOf(player[key].name) > -1 : val.indexOf(player[key]) > -1) {
                    cur_res = true;
                }
                res = res && cur_res;
            }
        }
        return res;
    }

    //helper function that returns filter data
    filterFunction(all_players) {
        return all_players
        .filter(player => this.findFilteredPlayer(player))
        .sort((a, b) => this.sortComparator(a, b))
        .map(player => [this.searchPlayerAttributes(player), player])
        .filter(playerTuple => {
            //remove teams from all fetched leauges whose names don't match the filtered keyword
            return playerTuple[0] > 0;
        })
        .sort((a, b) => b[0] - a[0])

        .map(playerTuple => {
            return (
                <PlayerCard 
                    player={playerTuple[1]} 
                    searchQuery = {this.state.filterText}
                />
            )
        })
    }

    sortComparator = (a, b) => {
        if (this.state.sorted){
            return a.player_name.trim().localeCompare(b.player_name.trim());
        }
        // return b.player_name.trim().localeCompare(a.player_name.trim());
    }

    // callback function for page change. passed to Pagination component
    onPageChanged = data => {
        const { all_players } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        // -1 accounts for zero-based indexing. We're fetching the next chunk of players to display
        const offset = (currentPage - 1) * pageLimit;
        const current_players = this.filterFunction(all_players).slice(offset, offset + pageLimit);

        // set current page to new 'page' of players. (essentially: Just changing what chunks of players get displayed) 
        this.setState({ currentPage, current_players, totalPages, offset, pageLimit });
    };

    toggleFilter = () => {
        let cur_filter = this.state.openFilter;
        this.setState({openFilter: !cur_filter, openSort: false});
    }

    toggleSort = () => {
        let cur_sort = this.state.openSort;
        this.setState({openSort: !cur_sort, openFilter: false});
    }

    handleFilterChange = (type, selectedOption) => {
        if (selectedOption === null) {
            selectedOption = [];
        }
        selectedOption = selectedOption.map(option => option.value);
        this.filterValUpdate(type, selectedOption);
    }

    displayDropdown = () => {
        const countries = [...new Set(this.state.all_players.map(players => players.nationality))]
                            .sort()
                            .map(nationality => ({ value: nationality, label: nationality }));
        const positions = [...new Set(this.state.all_players.map(players => players.position))]
                            .sort()
                            .map(posiiton => ({ value: posiiton, label: posiiton }));
        const teams = [...new Set(this.state.all_players.map(players => players.team.name))]
                            .sort()
                            .map(team => ({ value: team, label: team }));
        const leagues = [...new Set(this.state.all_players.map(players => players.league))]
                            .sort()
                            .map(league => ({ value: league, label: league }));

        return (
            <React.Fragment>
                Nationality:
                <Select
                    options={countries}
                    isMulti
                    onChange={e => this.handleFilterChange('nationality', e)}
                    components={makeAnimated()}
                />
                Position:
                <Select
                    options={positions}
                    isMulti
                    onChange={e => this.handleFilterChange('position', e)}
                    components={makeAnimated()}
                />
                Team:
                <Select
                    options={teams}
                    isMulti
                    onChange={e => this.handleFilterChange('team', e)}
                    components={makeAnimated()}
                />
                League:
                <Select
                    options={leagues}
                    isMulti
                    onChange={e => this.handleFilterChange('league', e)}
                    components={makeAnimated()}
                />
            </React.Fragment>
        );
    }

    handleToggle = () => {
        this.setState({sorted: !this.state.sorted, id: !this.state.id})
    }

    displaySort = () => {
        return (
            <div className="sort">
            <div className="sortText">Sort Alphabetically by Name</div>
              <input
                checked={this.state.sorted}
                onChange={this.handleToggle}
                className="react-switch-checkbox"
                id={`react-switch-new`}
                type="checkbox"
              />
              <label
                style={{ background: this.state.sorted && '#06D6A0' }}
                className="react-switch-label"
                htmlFor={`react-switch-new`}
              >
                <span className={`react-switch-button`} />
              </label>
            </div>
        )
    }



    render() {
        const {
            all_players,
            current_players,
            currentPage,
            totalPages,
            offset
        } = this.state;

        const modelName = "Players";
        let displayData = { modelName };
        let searchList = []

        if (this.state.done) {
            const total_players = all_players.length;
            if (total_players === 0) return null;
            let numDisplayed = offset + 1;

            // creates "list" of players filtered by search
            searchList = this.filterFunction(all_players)

            // group data to send together to component DisplayPageStats
            displayData = {
                numDisplayed,
                offset,
                searchList,
                totalPages,
                currentPage,
                modelName,
                current_instances: current_players
            };
        }

        console.log(all_players);

        return (
            <Container>

                {/* page header */}
                <Row className="space"></Row>
                <br></br>
                <br></br>
                <br></br>
                <Row className="about-row"><h1>PLAYERS</h1></Row>
                <br></br>

                {!this.state.done ? (
                    <div><Row>
                        <Loading loading={this.state.loading} />
                    </Row></div>
                ) : (
                    <FadeIn>
                        <div>
                            {/* page stats */}
                            <DisplayPageStats data={displayData} />
                            <br></br>
                            <br></br>

                            {/* search bar */}
                            <div className="filterTools">
                                <Search
                                    filterUpdate={this.filterUpdate.bind(this)}
                                    filterText={this.state.filterText}
                                    model={modelName}
                                    placeHolder="Enter a player's name, team, position, league, or age..."
                                />
                                <Button onClick={this.toggleFilter}> Filter </Button>
                                <Button onClick={this.toggleSort}> Sort </Button>
                                <br />
                                {
                                    this.state.openFilter ? this.displayDropdown() : this.state.openSort ? this.displaySort() : null
                                }
                            </div>

                            {/* model cards */}
                            <Row className="about text-center">
                                {current_players}
                            </Row>                   

                            <Row className="d-flex flex-row py-4 justify-content-center align-items-center">
                                <Pagination
                                    totalRecords={searchList.length}
                                    key={this.state.id}
                                    pageLimit={12}
                                    pageNeighbours={1}
                                    onPageChanged={this.onPageChanged}
                                />
                            </Row>
                            <br></br>
                            <br></br>
                        </div>
                    </FadeIn>
                )}
            </Container>
        );
    }
}

export default Players;
