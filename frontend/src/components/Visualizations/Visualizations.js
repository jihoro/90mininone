import React from 'react';
import * as d3 from 'd3';
import { Container, Row, Column } from 'react-bootstrap';
import "./Visualizations.css";

class Visualizations extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataLeagues : {},
            dataTeams : [],
            dataPlayers : [],
            xAttrLeague: "country",
            yAttrLeague: "numLeagues",
            xAttrTeam: "time",
            yAttrTeam: "num",
            xAttrPlayer: "age"   
        }
    }

    componentDidMount() {
        this.getDataLeagues();
        //this.getDataTeams();
    }

    componentDidUpdate() {
        //this.createBarChart();
        this.createPieChart();
        this.createBarChartTeam();
        this.createSP();
    }

//     async getDataTeams() {
//       var teams = [{"time": 1850, "num": 0},
//                             {"time": 1900, "num": 0},
//                             {"time": 1950, "num": 0},
//                             {"time": 2000, "num": 0},
//                             {"time": 2020, "num": 0},];

      
//       await d3.json("https://api.90mininone.me/Teams")
//           .then(response => {
//               console.log(response)
//               for (let i in response['teams']) {
//                  let model = response['teams'][i]
//                  console.log(model)
//                   //let modelAvgRating = this.calculateModelsSafetyRating(model)
//                   //if (modelAvgRating > 0) {
//                       for (let j in teams) {
//                         if(model.founded < teams[j].time) {
//                           teams[j].num += 1;
//                           break;
//                         }
//                           // case leagues[0].country:
//                           //     leagues[0].numLeagues += 1
//                           //     break
//                           // case leagues[1].country:
//                           //   leagues[1].numLeagues += 1
//                           //   break
//                       }
//                   //}

//               }
//           });
//       console.log(teams)
//       this.setState({dataTeams: teams});
//   }

    async getDataLeagues() {
        var leagues = {
                              "Brazil": 0,
                              "USA": 0,
                              
                              "Russia": 0,
                              "England": 0,
                              "France": 0,
                              "Germany": 0,
                              "Belgium": 0,
                              "Italy": 0,
                              "South-Korea": 0,
                              "Finland": 0,
                              "Japan": 0};
       // var leagues = {};
        
        await d3.json("https://api.90mininone.me/Leagues")
            .then(response => {
                //console.log(response)
                for (let i in response['leagues']) {
                   let model = response['leagues'][i]
                   //console.log(model)
                    //let modelAvgRating = this.calculateModelsSafetyRating(model)
                    //if (modelAvgRating > 0) {
                        // for (let j in leagues) {
                        //   if(model.country === leagues[j].country)
                        //     leagues[j].numLeagues += 1
                        //     // case leagues[0].country:
                        //     //     leagues[0].numLeagues += 1
                        //     //     break
                        //     // case leagues[1].country:
                        //     //   leagues[1].numLeagues += 1
                        //     //   break
                    
                            
                        // }
                        if(model.country in leagues) {
                            leagues[model.country] += 1;
                        }
                    //}

                }
            });
        console.log(leagues)
        var teams = [{"time": 1850, "num": 0},
                            {"time": 1900, "num": 0},
                            {"time": 1950, "num": 0},
                            {"time": 2000, "num": 0},
                            {"time": 2020, "num": 0},];

      
      await d3.json("https://api.90mininone.me/Teams")
          .then(response => {
              //console.log(response)
              for (let i in response['teams']) {
                 let model = response['teams'][i]
                 //console.log(model)
                  //let modelAvgRating = this.calculateModelsSafetyRating(model)
                  //if (modelAvgRating > 0) {
                      for (let j in teams) {
                        if(model.founded < teams[j].time) {
                          teams[j].num += 1;
                          break;
                        }
                          // case leagues[0].country:
                          //     leagues[0].numLeagues += 1
                          //     break
                          // case leagues[1].country:
                          //   leagues[1].numLeagues += 1
                          //   break
                      }
                  //}

              }
          });
      //console.log(teams)
      var players = [{"age": 20, "num": 0},
                            {"age": 30, "num": 0},
                            {"age": 40, "num": 0},
                            {"age": 18, "num": 0}];

      
      await d3.json("https://api.90mininone.me/Players")
          .then(response => {
              //console.log(response)
              for (let i in response['players']) {
                 let model = response['players'][i]
                 //console.log(model)
                  //let modelAvgRating = this.calculateModelsSafetyRating(model)
                  //if (modelAvgRating > 0) {
                      var found =  false
                      for (let j in players) {
                        if(model.age === players[j].age) {
                          players[j].num += 1;
                          found = true;
                          break;
                        }
                          // case leagues[0].country:
                          //     leagues[0].numLeagues += 1
                          //     break
                          // case leagues[1].country:
                          //   leagues[1].numLeagues += 1
                          //   break
                      }
                      if(!found &&  model.age != 0) {
                        players.push({
                            age:   model.age,
                            num: 1
                        });
                      }
                  //}

              }
          });
      //console.log(players)
      this.setState({dataLeagues: leagues, dataTeams: teams, dataPlayers : players});

      
    }

    createPieChart() {
        var width = 600
    var height = 600
    var margin = 40

// The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
var radius = Math.min(width, height) / 2 - margin

// append the svg object to the div called 'my_dataviz'
var svg = d3.select(this.refs.pieChart)
  .append("svg")
    .attr("width", width)
    .attr("height", height)
  .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

// Create dummy data
var data = this.state.dataLeagues;
// set the color scale
var color = d3.scaleOrdinal()
  .domain(data)
  .range(d3.schemeSet2);

// Compute the position of each group on the pie:
var pie = d3.pie()
  .value(function(d) {return d.value; })
var data_ready = pie(d3.entries(data))
// Now I know that group A goes from 0 degrees to x degrees and so on.

// shape helper to build arcs:
var arcGenerator = d3.arc()
  .innerRadius(0)
  .outerRadius(radius)

// Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
svg
  .selectAll('mySlices')
  .data(data_ready)
  .enter()
  .append('path')
    .attr('d', arcGenerator)
    .attr('fill', function(d){ return(color(d.data.key)) })
    .attr("stroke", "black")
    .style("stroke-width", "2px")
    .style("opacity", 0.7)

// Now add the annotation. Use the centroid method to get the best coordinates
svg
  .selectAll('mySlices')
  .data(data_ready)
  .enter()
  .append('text')
  .text(function(d){ return  d.data.key})
  .attr("transform", function(d) { return "translate(" + arcGenerator.centroid(d) + ")";  })
  .style("text-anchor", "middle")
  .style("font-size", 17)

      }
    
    createSP() {
        const margin = { top: 100, right: 20, bottom: 100, left: 60 };
        const width = 1200 - margin.left - margin.right;
        const height = 600 - margin.top - margin.bottom;
    
        const svg = d3
          .select(this.refs.sp)
          .append('svg')
          .attr('width', width + margin.left + margin.right)
          .attr('height', height + margin.top + margin.bottom)
          .append('g')
          .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
    
        const x = d3
          .scaleLinear()
          .domain([0, 50])
          .range([0, width]);
        svg
          .append('g')
          .attr('transform', 'translate(0,' + height + ')')
          .call(d3.axisBottom(x));
    
        const y = d3
          .scaleLinear()
          .domain([0, 300])
          .range([height, 0]);
        svg.append('g').call(d3.axisLeft(y));
    
        svg
          .append('text')
          .attr('text-anchor', 'end')
          .attr('x', width / 2 + margin.left)
          .attr('y', height + margin.top - 30)
          .text('Age of Players')
          .style('fill', '#ff6347');
    
        svg
          .append('text')
          .attr('text-anchor', 'end')
          .attr('transform', 'rotate(-90)')
          .attr('y', -margin.left + 17)
          .attr('x', -margin.top - height / 2 + 100)
          .text('Number of Players')
          .style('fill', '#ff6347');
    
        svg
          .append('g')
          .selectAll('dot')
          .data(this.state.dataPlayers)
          .enter()
          .append('circle')
          .attr('cx', d => x(d[this.state.xAttrPlayer]))
          .attr('cy', d => y(d[this.state.yAttrTeam]))
          .attr('r', 3)
          .style('fill', '#ff6347');
      }
    

    createBarChart() {
        const margin = { top: 200, right: 20, bottom: 100, left: 60 };
        const height = 600 - margin.top - margin.bottom;
        const width = 1200 - margin.left - margin.right;

        const svg = d3
            .select(this.refs.leaguePerCountry)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        const x = d3
            .scaleBand()
            .range([0, width])
            .domain(this.state.dataLeagues.map(d => d[this.state.xAttrLeague]))
            .padding(0.2);
        svg.append('g')
            .attr('transform', 'translate(0,' + height + ')')
            .call(d3.axisBottom(x))
            .selectAll('text')
            .attr('transform', 'translate(-10,0)rotate(-45)')
            .style('text-anchor', 'end');

        const maxValue = Math.max(...this.state.dataLeagues.map(d => d[this.state.yAttrLeague]));
        const y = d3
            .scaleLinear()
            .domain([0, Math.ceil(maxValue / 3) * 3])
            .range([height, 0]);
        svg.append('g').call(d3.axisLeft(y));

        svg.selectAll('mybar')
            .data(this.state.dataLeagues)
            .enter()
            .append('rect')
            .attr('x', d => x(d[this.state.xAttrLeague]))
            .attr('y', d => y(d[this.state.yAttrLeague]))
            .attr('width', x.bandwidth())
            .attr('height', d => height - y(d[this.state.yAttrLeague]))
            .attr('fill', 'tomato')
    }

    createBarChartTeam() {
      const margin = { top: 200, right: 20, bottom: 100, left: 60 };
      const height = 600 - margin.top - margin.bottom;
      const width = 1200 - margin.left - margin.right;

      const svg = d3
          .select(this.refs.teamsPerTime)
          .append('svg')
          .attr('width', width + margin.left + margin.right)
          .attr('height', height + margin.top + margin.bottom)
          .append('g')
          .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

      const x = d3
          .scaleBand()
          .range([0, width])
          .domain(this.state.dataTeams.map(d => d[this.state.xAttrTeam]))
          .padding(0.2);
      svg.append('g')
          .attr('transform', 'translate(0,' + height + ')')
          .call(d3.axisBottom(x))
          .selectAll('text')
          .attr('transform', 'translate(-10,0)rotate(-45)')
          .style('text-anchor', 'end');

      const maxValue = Math.max(...this.state.dataTeams.map(d => d[this.state.yAttrTeam]));
      const y = d3
          .scaleLinear()
          .domain([0, Math.ceil(maxValue / 5) * 5])
          .range([height, 0]);
      svg.append('g').call(d3.axisLeft(y));

      svg.selectAll('mybar')
          .data(this.state.dataTeams)
          .enter()
          .append('rect')
          .attr('x', d => x(d[this.state.xAttrTeam]))
          .attr('y', d => y(d[this.state.yAttrTeam]))
          .attr('width', x.bandwidth())
          .attr('height', d => height - y(d[this.state.yAttrTeam]))
          .attr('fill', 'tomato')
  }
    render() {
        return (
            <Container className="text-center" style={{display:'flex', justifyContent:'center', flexDirection:'column'}}>
                <div className="spacer2"></div>

                <div><h1>VISUALIZATIONS</h1></div>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <div><h3>Number of Leagues by Country</h3></div>
                <div class = "input" ref="pieChart"></div>

                <div className="bigSpace"></div>

                <div><h3>Years that Teams were Founded</h3></div>
                <div ref="teamsPerTime" ></div>

                <div className="bigSpace"></div>

                <div><h3>Age of Players</h3></div>
                <div ref="sp"></div>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
            </Container>
        );
    }
}

export default Visualizations;
