import React from 'react';
import * as d3 from 'd3';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import "./Provider.css";
import {Container} from 'react-bootstrap';
class Provider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataPets : {},
            dataBreeds : [],
            dataShelters : [],
            xAttr: "age",
            yAttr: "num",
            xAttrShelter: "state" 
        }
    }

    componentDidMount() {
        this.getDataLeagues();
        //this.getDataTeams();
    }

    componentDidUpdate() {
        //this.createBarChart();
        this.createBarChartTeam();
        this.createPieChart();
        //this.createSP();
    }

//     async getDataTeams() {
//       var teams = [{"time": 1850, "num": 0},
//                             {"time": 1900, "num": 0},
//                             {"time": 1950, "num": 0},
//                             {"time": 2000, "num": 0},
//                             {"time": 2020, "num": 0},];

      
//       await d3.json("https://api.90mininone.me/Teams")
//           .then(response => {
//               console.log(response)
//               for (let i in response['teams']) {
//                  let model = response['teams'][i]
//                  console.log(model)
//                   //let modelAvgRating = this.calculateModelsSafetyRating(model)
//                   //if (modelAvgRating > 0) {
//                       for (let j in teams) {
//                         if(model.founded < teams[j].time) {
//                           teams[j].num += 1;
//                           break;
//                         }
//                           // case leagues[0].country:
//                           //     leagues[0].numLeagues += 1
//                           //     break
//                           // case leagues[1].country:
//                           //   leagues[1].numLeagues += 1
//                           //   break
//                       }
//                   //}

//               }
//           });
//       console.log(teams)
//       this.setState({dataTeams: teams});
//   }

    async getDataLeagues() {
        var pets = {Young : 0, Baby: 0, Adult : 0};

        
        await d3.json("https://api.pets4.me/api/pets")
            .then(response => {
                //console.log(response)
                for (let i in response['objects']) {
                   let model = response['objects'][i]
                   //console.log(model)
                    //let modelAvgRating = this.calculateModelsSafetyRating(model)
                    //if (modelAvgRating > 0) {
                        //for (let j in pets) {
                          if(model.age in pets) {
                            pets[model.age] += 1;
                          }
                            // case leagues[0].country:
                            //     leagues[0].numLeagues += 1
                            //     break
                            // case leagues[1].country:
                            //   leagues[1].numLeagues += 1
                            //   break
                    
                            
                        //}
                    //}

                }
            });
        //console.log(pets)
        var shelters = [];

      
      await d3.json("https://api.pets4.me/api/shelters")
          .then(response => {
              //console.log(response)
              for (let i in response['objects']) {
                 let model = response['objects'][i]
                 //console.log(model)
                  //let modelAvgRating = this.calculateModelsSafetyRating(model)
                  //if (modelAvgRating > 0) {
                    var found =  false
                    for (let j in shelters) {
                      if(model.address.city === shelters[j].label) {
                        shelters[j].value += 1;
                        found = true;
                        break;
                      }
                        // case leagues[0].country:
                        //     leagues[0].numLeagues += 1
                        //     break
                        // case leagues[1].country:
                        //   leagues[1].numLeagues += 1
                        //   break
                    }
                    if(!found) {
                      shelters.push({
                          label: model.address.city,
                          value: 1
                      });
                    }

              }
          });

      console.log(shelters)
      //console.log(window.innerWidth)
      var breeds = [];

      
      await d3.json("https://api.pets4.me/api/dog_breeds")
          .then(response => {
              //console.log(response)
              for (let i in response['objects']) {
                 let model = response['objects'][i]
                 //console.log(model)
                  //let modelAvgRating = this.calculateModelsSafetyRating(model)
                  //if (modelAvgRating > 0) {
                      var found =  false
                      for (let j in breeds) {
                        if(model.life_span.high === breeds[j].age) {
                          breeds[j].num += 1;
                          found = true;
                          break;
                        }
                          // case leagues[0].country:
                          //     leagues[0].numLeagues += 1
                          //     break
                          // case leagues[1].country:
                          //   leagues[1].numLeagues += 1
                          //   break
                      }
                      if(!found) {
                        breeds.push({
                            age: model.life_span.high,
                            num: 1
                        });
                      }
                  //}

              }
          });
      //console.log(breeds)
      this.setState({dataPets: pets, dataBreeds: breeds, dataShelters : shelters});

      
    }
    createPieChart() {
        var width = 500
    var height = 500
    var margin = 40

// The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
var radius = Math.min(width, height) / 2 - margin

// append the svg object to the div called 'my_dataviz'
var svg = d3.select(this.refs.pieChart)
  .append("svg")
    .attr("width", width)
    .attr("height", height)
  .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

// Create dummy data
var data = this.state.dataPets;
// set the color scale
var color = d3.scaleOrdinal()
  .domain(data)
  .range(d3.schemeSet2);

// Compute the position of each group on the pie:
var pie = d3.pie()
  .value(function(d) {return d.value; })
var data_ready = pie(d3.entries(data))
// Now I know that group A goes from 0 degrees to x degrees and so on.

// shape helper to build arcs:
var arcGenerator = d3.arc()
  .innerRadius(0)
  .outerRadius(radius)

// Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
svg
  .selectAll('mySlices')
  .data(data_ready)
  .enter()
  .append('path')
    .attr('d', arcGenerator)
    .attr('fill', function(d){ return(color(d.data.key)) })
    .attr("stroke", "black")
    .style("stroke-width", "2px")
    .style("opacity", 0.7)

// Now add the annotation. Use the centroid method to get the best coordinates
svg
  .selectAll('mySlices')
  .data(data_ready)
  .enter()
  .append('text')
  .text(function(d){ return  d.data.key})
  .attr("transform", function(d) { return "translate(" + arcGenerator.centroid(d) + ")";  })
  .style("text-anchor", "middle")
  .style("font-size", 17)

      }
    // createSP() {
    //     const margin = { top: 100, right: 20, bottom: 100, left: 60 };
    //     const width = 1200 - margin.left - margin.right;
    //     const height = 600 - margin.top - margin.bottom;
    
    //     const svg = d3
    //       .select(this.refs.sp)
    //       .append('svg')
    //       .attr('width', width + margin.left + margin.right)
    //       .attr('height', height + margin.top + margin.bottom)
    //       .append('g')
    //       .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
    
    //       const x = d3
    //       .scaleBand()
    //       .range([0, width])
    //       .domain(this.state.dataShelters.map(d => d[this.state.xAttrShelter]))
    //       .padding(0.2);
    //   svg.append('g')
    //       .attr('transform', 'translate(0,' + height + ')')
    //       .call(d3.axisBottom(x))
    //       .selectAll('text')
    //       .attr('transform', 'translate(-10,0)rotate(-45)')
    //       .style('text-anchor', 'end');

    //   const maxValue = Math.max(...this.state.dataShelters.map(d => d[this.state.yAttr]));
    //   const y = d3
    //       .scaleLinear()
    //       .domain([0, Math.ceil(maxValue / 3) * 3])
    //       .range([height, 0]);
    //   svg.append('g').call(d3.axisLeft(y));

    //   svg.selectAll('mybar')
    //       .data(this.state.dataShelters)
    //       .enter()
    //       .append('rect')
    //       .attr('x', d => x(d[this.state.xAttrShelter]))
    //       .attr('y', d => y(d[this.state.yAttr]))
    //       .attr('width', x.bandwidth())
    //       .attr('height', d => height - y(d[this.state.yAttr]))
    //       .attr('fill', 'tomato')
    //   }
    

    // createBarChart() {
    //     const margin = { top: 200, right: 20, bottom: 100, left: 60 };
    //     const height = 600 - margin.top - margin.bottom;
    //     const width = 1200 - margin.left - margin.right;

    //     const svg = d3
    //         .select(this.refs.leaguePerCountry)
    //         .append('svg')
    //         .attr('width', width + margin.left + margin.right)
    //         .attr('height', height + margin.top + margin.bottom)
    //         .append('g')
    //         .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    //     const x = d3
    //         .scaleBand()
    //         .range([0, width])
    //         .domain(this.state.dataPets.map(d => d[this.state.xAttr]))
    //         .padding(0.2);
    //     svg.append('g')
    //         .attr('transform', 'translate(0,' + height + ')')
    //         .call(d3.axisBottom(x))
    //         .selectAll('text')
    //         .attr('transform', 'translate(-10,0)rotate(-45)')
    //         .style('text-anchor', 'end');

    //     const maxValue = Math.max(...this.state.dataPets.map(d => d[this.state.yAttr]));
    //     const y = d3
    //         .scaleLinear()
    //         .domain([0, Math.ceil(maxValue / 3) * 3])
    //         .range([height, 0]);
    //     svg.append('g').call(d3.axisLeft(y));

    //     svg.selectAll('mybar')
    //         .data(this.state.dataPets)
    //         .enter()
    //         .append('rect')
    //         .attr('x', d => x(d[this.state.xAttr]))
    //         .attr('y', d => y(d[this.state.yAttr]))
    //         .attr('width', x.bandwidth())
    //         .attr('height', d => height - y(d[this.state.yAttr]))
    //         .attr('fill', 'tomato')
    // }

    createBarChartTeam() {
      const margin = { top: 50, right: 20, bottom: 50, left: 60 };
      const height = 500 - margin.top - margin.bottom;
      const width = 1200 - margin.left - margin.right;

      const svg = d3
          .select(this.refs.teamsPerTime)
          .append('svg')
          .attr('width', width + margin.left + margin.right)
          .attr('height', height + margin.top + margin.bottom)
          .append('g')
          .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

      const x = d3
          .scaleBand()
          .range([0, width])
          .domain(this.state.dataBreeds.map(d => d[this.state.xAttr]))
          .padding(0.2);
      svg.append('g')
          .attr('transform', 'translate(0,' + height + ')')
          .call(d3.axisBottom(x))
          .selectAll('text')
          .attr('transform', 'translate(-10,0)rotate(-45)')
          .style('text-anchor', 'end');

      const maxValue = Math.max(...this.state.dataBreeds.map(d => d[this.state.yAttr]));
      const y = d3
          .scaleLinear()
          .domain([0, Math.ceil(maxValue / 5) * 5])
          .range([height, 0]);
      svg.append('g').call(d3.axisLeft(y));

      svg.selectAll('mybar')
          .data(this.state.dataBreeds)
          .enter()
          .append('rect')
          .attr('x', d => x(d[this.state.xAttr]))
          .attr('y', d => y(d[this.state.yAttr]))
          .attr('width', x.bandwidth())
          .attr('height', d => height - y(d[this.state.yAttr]))
          .attr('fill', 'tomato')
  }
    render() {
        return <Container className="text-center" style={{display:'flex', justifyContent:'center', flexDirection:'column'}}>
         <div className="spacer2"></div>

<div><h1>Provider Visualizations - Pets4Me</h1></div>
<br></br>
<br></br>
<br></br>
<br></br>
<div><h3>Life Span of Dog Breeds</h3></div>
         <div ref="teamsPerTime"></div>
         <div className="bigSpace"></div>

        <div><h3>Age Population of Pets at Shelters</h3></div>
         <div ref="pieChart"></div>
         {/* bubbleClick = (label) =>{
  console.log("Custom bubble click func")
}
legendClick = (label) =>{
  console.log("Customer legend click func") */}
  <div className="bigSpace"></div>

<div><h3>Shelter Locations</h3></div>
         <BubbleChart
  graph= {{
    zoom: 0.7,
    offsetX: -0.05,
    offsetY: -0.01,
  }}
  width={1000}
  height={800}
  padding={0} // optional value, number that set the padding between bubbles
  showLegend={true} // optional value, pass false to disable the legend.
  legendPercentage={10} // number that represent the % of with that legend going to use.
  legendFont={{
        family: 'Arial',
        size: 12,
        color: '#000',
        weight: 'bold',
      }}
  valueFont={{
        family: 'Arial',
        size: 12,
        color: '#fff',
        weight: 'bold',
      }}
  labelFont={{
        family: 'Arial',
        size: 16,
        color: '#fff',
        weight: 'bold',
      }}
  //Custom bubble/legend click functions such as searching using the label, redirecting to other page
//   bubbleClickFunc={this.bubbleClick}
//   legendClickFun={this.legendClick}
  data= {this.state.dataShelters}
/>
</Container>;
    }
}

export default Provider;
