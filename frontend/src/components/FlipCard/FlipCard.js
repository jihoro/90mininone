import React, { Component, useState } from "react";
import {Button} from "react-bootstrap";
import { useSpring, animated as a } from 'react-spring'
import './FlipCard.css'


function FlipCard(props) {
    const [flipped, set] = useState(false)
    const { transform, opacity } = useSpring({
        opacity: flipped ? 1 : 0,
        transform: `perspective(600px) rotateX(${flipped ? 180 : 0}deg)`,
        config: { mass: 5, tension: 500, friction: 80 }
    })
    return (
        <div className="parent-div" onMouseEnter={() => set(state => true)} onMouseLeave={() => set(state => false)}>
            <a.div className="c back b" style={{
                opacity: opacity.interpolate(o => 1 - o),
                backgroundImage: 'url(' + props.image + ')',
                transform
            }} />

            <a.div className="c front b" style={{
                opacity,
                transform: transform.interpolate(t => `${t} rotateX(180deg)`),
                boxShadow: '5px 5px 15px tomato'
            }}>
                <Button className="card-link" href={props.goTo}>
                    <h4>{props.title}</h4>
                    <div>{props.description}</div>
                </Button>
            </a.div>


        </div>
    )
}
export default FlipCard;


