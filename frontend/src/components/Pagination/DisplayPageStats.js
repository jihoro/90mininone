import React from 'react'

//displays page data
const displayPageData = data => {

    const { currentPage, totalPages, current_instances, numDisplayed, offset, searchList, modelName = ""} = data;

    // case for search results has at least 1 instance a.k.a current page is not 0
    if (currentPage) {
        return (
            <div className="d-flex flex-column justify-content-center align-items-center">
                <span className="current-page d-inline-block h-100 text-secondary">
                    Page: &nbsp;&nbsp;
                    <span className="font-weight-bold">{currentPage}</span> /{" "}
                    <span className="font-weight-bold">{totalPages}</span>
                </span>
                <span className="current-page d-inline-block h-100 text-secondary">
                    Displaying 
                    <span>{" " + modelName + ":"} </span> &nbsp;&nbsp;
                    <span className="font-weight-bold">{numDisplayed}</span> {" "}
                    {" - "}
                    <span className="font-weight-bold">{offset + current_instances.length}</span> {" "}
                    of{" "}
                    <span className="font-weight-bold">{searchList.length}</span> {" "}
                </span>
            </div>
        )
    }

    // case for zero search results a.k.a current page is 0
    return (
        <div className="d-flex flex-column justify-content-center align-items-center">
            <span className="current-page d-inline-block h-100 text-secondary">
                Page: &nbsp;&nbsp;
            <span className="font-weight-bold"> no pages</span>
            </span>
            <span className="current-page d-inline-block h-100 text-secondary">
                Displaying 
                <span>{" " + modelName + ":"} </span> &nbsp;&nbsp;
                <span className="font-weight-bold"> none to display </span>
            </span>
        </div>
    )
}

export default (props) => {
    return (
        displayPageData(props.data)
    );
}

