import React, { Component } from 'react';
import { Container, Row, Button } from 'react-bootstrap';
import Pagination from "../Pagination/Pagination";
import DisplayPageStats from "../Pagination/DisplayPageStats";
import Search from "../Search";
import Loading from "../Loading/Loading";
import FadeIn from "react-fade-in";
import LeagueCard from "./LeagueCard";
import "./Leagues.css"
import baseUrl from '../../assets/baseUrl';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';


class Leagues extends Component {

    constructor(props) {
        super(props)
        this.state = {
            all_leagues: [],
            current_leagues: [],
            currentPage: null,
            totalPages: null,
            offset: 0,
            pageLimit: 0,
            filterText: '',
            filterVals: {},
            id: 0,
            done: undefined,
            loading: undefined,
            sorted: false,
            openFilter: false,
            openSort: false
        }
    }

    componentDidMount() {
        setTimeout(() => {
            fetch(baseUrl + "/Leagues")
                .then(response => response.json())
                .then(data => {
                    this.setState({ loading: true });
                    setTimeout(() => {
                        this.setState({ done: true, all_leagues: data.leagues });
                    }, 2000);
                });
        }, 1200);
    }

    filterUpdate(value) {
        this.setState({
            filterText: value,

            // id used to make new paginatgion bar and pages on search instead of trying to update it.
            // changing some prop (this one) to pagination component every time should force this action
            // has to do with react component state based on prop. so lifecycle events require this
            id: !this.state.id
        })
    }

    filterValUpdate(key, value) {
        this.state.filterVals[key] = value;
        this.setState({
            id: !this.state.id
        })
    }

    //helper function to check whether searchable league attirbutes match search query
    // EDIT this when adding/removing attributes we want to match search to
    searchLeagueAttributes(league) {
        const search = this.state.filterText.toLowerCase();
        if (search.length === 0) {
            return 1;
        }
        const keywords = search.trim().split(' ');

        const name = league.name.toLowerCase();
        const cc = league.country_code.toLowerCase();
        const country = league.country.toLowerCase();

        let score = 0;

        // loop through search keywords and include if keyword present in attributes
        for (let i = 0; i < keywords.length; i++) {
            let word = keywords[i];

            let nameMatch = name.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let ccMatch = cc.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;
            let countryMatch = country.split(' ').find(seg => seg.startsWith(word)) ? 1 : 0;

            score += nameMatch + ccMatch + countryMatch;
        }

        return score;
    }

    findFilteredLeague(league) {
        let res = true;
        for(const [key, val] of Object.entries(this.state.filterVals)) {
            if (val.length > 0) {
                let cur_res = false;
                if (val.indexOf(league[key]) > -1) {
                    cur_res = true;
                }
                res = res && cur_res;
            }
        }
        return res;
    }

    //helper function that returns filter data
    filterFunction(all_leagues) {
        const search = this.state.filterText.toLowerCase();
        console.log(search);
        return all_leagues
            .filter(league => this.findFilteredLeague(league))
            .sort((a, b) => this.sortComparator(a, b))
            .map(league => [this.searchLeagueAttributes(league), league])
            .filter(leagueTuple => {
                //remove leagues from all fetched leauges whose names don't match the filtered keyword
                return leagueTuple[0] > 0;
            })
            .sort((a, b) => b[0] - a[0])
            .map(leagueTuple => {
                return (
                    <LeagueCard 
                        league={leagueTuple[1]} 
                        searchQuery = {this.state.filterText.toLowerCase()}
                    />
                )
            })
    }

    sortComparator = (a, b) => {
        if (this.state.sorted){
            return a.name.trim().localeCompare(b.name.trim());
        }
        // return b.name.trim().localeCompare(a.name.trim());
    }

    // callback function for page change. passed to Pagination component
    onPageChanged = data => {
        const { all_leagues } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        // -1 accounts for zero-based indexing. We're fetching the next chunk of leagues to display
        const offset = (currentPage - 1) * pageLimit;
        const current_leagues = this.filterFunction(all_leagues).slice(offset, offset + pageLimit);

        // set current page to new 'page' of leagues. (essentially: Just changing what chunks of leagues get displayed) 
        this.setState({ currentPage, current_leagues, totalPages, offset, pageLimit });
    };

    toggleFilter = () => {
        let cur_filter = this.state.openFilter;
        this.setState({openFilter: !cur_filter, openSort: false});
    }

    toggleSort = () => {
        let cur_sort = this.state.openSort;
        this.setState({openSort: !cur_sort, openFilter: false});
    }

    handleCountryFilterChange = (selectedOption) => {
        if (selectedOption === null) {
            selectedOption = [];
        }
        selectedOption = selectedOption.map(option => option.value);
        this.filterValUpdate('country', selectedOption);
    }

    handleStatusFilterChange = (selectedOption) => {
        if (selectedOption === null) {
            selectedOption = [];
        }
        selectedOption = selectedOption.map(option => option.value);
        this.filterValUpdate('standings', selectedOption);
    }

    displayDropdown = () => {
        const countries = [...new Set(this.state.all_leagues.map(league => league.country))]
                            .sort()
                            .map(country => ({ value: country, label: country }));

        return (
            <React.Fragment>
                Country:
                <Select
                    options={countries}
                    isMulti
                    onChange={this.handleCountryFilterChange}
                    components={makeAnimated()}
                />
                League Status:
                <Select
                    options={[{ value: true, label: 'Standings available' }, { value: false, label: 'Standings not available' }]}
                    isMulti
                    onChange={this.handleStatusFilterChange}
                    components={makeAnimated()}
                />
            </React.Fragment>
        );
    }

    handleToggle = () => {
        this.setState({sorted: !this.state.sorted, id: !this.state.id})
    }

    displaySort = () => {
        return (
            <div className="sort">
            <div className="sortText">Sort Alphabetically by Name</div>
              <input
                checked={this.state.sorted}
                onChange={this.handleToggle}
                className="react-switch-checkbox"
                id={`react-switch-new`}
                type="checkbox"
              />
              <label
                style={{ background: this.state.sorted && '#06D6A0' }}
                className="react-switch-label"
                htmlFor={`react-switch-new`}
              >
                <span className={`react-switch-button`} />
              </label>
            </div>
        )
    }

    render() {
        const {
            all_leagues,
            current_leagues,
            currentPage,
            totalPages,
            offset
        } = this.state;

        const modelName = "Leagues";
        let displayData = { modelName };
        let searchList = []

        if (this.state.done) {
            const total_leagues = all_leagues.length;
            if (total_leagues === 0) return null;
            let numDisplayed = offset + 1;

            // creates "list" of leeagues filtered by search
            searchList = this.filterFunction(all_leagues)

            // group data to send together to component DisplayPageStats
            displayData = {
                numDisplayed,
                offset,
                searchList,
                totalPages,
                currentPage,
                modelName,
                current_instances: current_leagues
            };
        }

        console.log(all_leagues);

        return (
            <Container>

                {/* page header */}
                <Row className="space"></Row>
                <br></br>
                <br></br>
                <br></br>
                <Row className="about-row"><h1>LEAGUES</h1></Row>
                <br></br>


                {!this.state.done ? (
                    <div><Row>
                        <Loading loading={this.state.loading} />
                    </Row></div>
                ) : (
                    <FadeIn>
                        <div>
                            {/* page stats */}
                            <DisplayPageStats data={displayData} />
                            <br></br>
                            <br></br>

                            {/* search bar */}
                            <div className="filterTools">
                                <Search
                                    filterUpdate={this.filterUpdate.bind(this)}
                                    filterText={this.state.filterText}
                                    model={modelName}
                                    placeHolder="Enter a league's name, country, or country code..."
                                />
                                <Button onClick={this.toggleFilter}> Filter </Button>
                                <Button onClick={this.toggleSort}> Sort </Button>
                                <br />
                                {
                                    this.state.openFilter ? this.displayDropdown() : this.state.openSort ? this.displaySort() : null
                                }
                            </div>

                            {/* model cards */}
                            <Row className="about text-center">
                                {current_leagues}
                            </Row>
                        

                            {/* pagination bar */}
                            <Row className="d-flex flex-row py-4 justify-content-center align-items-center">
                                <Pagination
                                    totalRecords={searchList.length}
                                    key={this.state.id}
                                    pageLimit={12}
                                    pageNeighbours={1}
                                    onPageChanged={this.onPageChanged}
                                />
                            </Row>
                            <br></br>
                            <br></br>
                        </div>
                    </FadeIn>
                )}
            </Container>
        );
    }
}

export default Leagues;
