import React, { useState, useEffect } from 'react';
import { Image, Container, Row, ListGroup, Col, Accordion, Card, Button } from 'react-bootstrap';
import baseUrl from '../../assets/baseUrl';
import InfiniteScroll from "react-infinite-scroll-component";
import { Link } from "react-router-dom";
import "./Leagues.css"

const style = {
    height: 30,
    border: "1px solid green",
    margin: 6,
    padding: 8
  };

const LeagueInstancePage = (props) => {
    const league_id = props.location.state.league_id;
    console.log(league_id)

    const [league, setLeague] = useState({});
    const [teams, setTeams] = useState(null);

    async function fetchLeague() {
        const res = await fetch(baseUrl + "/Leagues/" + league_id);
        res
            .json()
            .then(res => setLeague(res));
    }

    useEffect(() => {
        fetchLeague();
    }, []);

    const { 
        logo = null, 
        name = " ", 
        country, 
        flag, 
        country_code,
        season_start,
        season_end,
        standings,
        current
    } = league;

    //fetches league based on team_id and sets state "league"
    async function fetchTeams(name) {
        console.log(name);
        const res = await fetch(baseUrl + "/TeamAndLeague?league=" + name);
        res
            .json()
            .then(res => setTeams(res));
    }

    //use effect that gets called when team_id changes from 0 to x. Calls fetchLeagues()
    useEffect(() => {
        fetchTeams(name);
    }, [name]);



    return (
        <div className="background">
        <Container className="instanceCon">
            <Row className="space"></Row>
            <br></br>
            <br></br>
            <br></br>
            <Row className="teamRowContainer">
                <Col md={4} className="leagueStatsCard">
                    <Row>
                        <div className="d-flex justify-content-center align-items-center text-center">
                            <h1 className="text-center">{name.toUpperCase()}</h1>
                        </div>
                    </Row>
                    <br></br>
                    <Row>
                        <ListGroup variant="flush">
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Country: &#160;</div>
                                <div >{country}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Country Code: &#160;</div>
                                <div >{country_code}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Season Start: &#160;</div>
                                <div >{season_start}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Season End: &#160;</div>
                                <div >{season_end}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Season End: &#160;</div>
                                <div >{season_end}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Standings: &#160;</div>
                                <div >{standings ? 'Yes' : 'No'}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Current: &#160;</div>
                                <div >{current ? 'Yes' : 'No'}</div>
                            </ListGroup.Item>
                        </ListGroup>
                    </Row>
                </Col>

                <Col md={4} className="d-flex flex-column align-items-center">
                    <Row className="imageCardLink">
                        <Image className="leagueImage" src={logo} rounded fluid/>
                    </Row>
                    <br></br>
                    <br></br>
                    <Row className="imageCardLink">
                        <Image className="flagImage" src={flag} rounded fluid/>
                    </Row>
                </Col>

                <Col md={4}>
                    <div className="teamsCardLink flex-column">
                        <h3>TEAMS</h3>
                        <br></br>
                        <InfiniteScroll
                            dataLength={teams != null && teams.length != 0 ? teams.length : 0}
                            style={{display:'inline-flex', maxHeight: '400px', overflow: 'auto', height:'auto'}}
                            height={400}
                            >
                            {teams != null && teams.length != 0 ? (
                                    <div>
                                        {teams
                                            .filter(x => {return x.team != 'None'})
                                            .map(team => (
                                                <Link className="linktext" to={{
                                                    pathname: '/Teams/' + team.team.replace(' ', ''),
                                                    state: {
                                                        team_id: team.team_id
                                                    }
                                                }}>
                                                    <p>{team.team}</p>
                                                </Link>
                                            ))
                                        }
                                    </div>
                                ) : 'no realted team data to display'}
                        </InfiniteScroll>
                    </div>
                </Col>
            </Row>
        </Container>
        </div>
    );
}

export default LeagueInstancePage;