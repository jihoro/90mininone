import React from "react";
import { Col, Card, ListGroup, ListGroupItem, Button, Image } from 'react-bootstrap';
import Highlight from "../Highlight/Highlight";
import { Link } from "react-router-dom";
import "./Leagues.css";

const LeagueCard  = (props) =>  {
    var { league_id = {}, name = {}, country = null, country_code = null, logo = {}, flag = {} } = props.league;
    const searchQuery = props.searchQuery;

    const nameHighlight = <Highlight searchQuery = {searchQuery} word = {name} />
    const ccHighlight = <Highlight searchQuery = {searchQuery} word = {country_code} />
    const countryHighlight = <Highlight searchQuery = {searchQuery} word = {country} />

    return (
        <Col lg={3} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <Card className="league-card">

                {/* League Logo */}
                <div className="image-card">     
                        <Image className="league-logo" src={logo} rounded/>
                </div>

                <Card.Body className="cb">
                    <Card.Title> {nameHighlight}</Card.Title>
                    <ListGroup className="list-group-flush">

                        {/* Country */}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}>Country: &#160;</div>
                            <div style={{ display: 'inline-block' }}>{countryHighlight}</div>
                        </ListGroupItem>

                        {/* Country Code*/}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}>
                                Country Code: &#160;
                            </div>
                            <div style={{ display: 'inline-block' }}>{ccHighlight}</div>
                        </ListGroupItem>

                        {/* Flag */}
                        <ListGroupItem>
                            <img src={flag} width="30"></img>
                        </ListGroupItem>

                        {/* More Info button */}
                        <ListGroupItem>
                            <Link className="linktext" to={{
                                pathname: '/Leagues/' + name.replace(' ', ''),
                                state: {
                                    league_id: props.league.league_id
                                }
                                }}>
                                <Button>More Info</Button>
                            </Link>
                        </ListGroupItem>

                    </ListGroup>
                </Card.Body>
            </Card>
        </Col>
    );
}

export default LeagueCard;

