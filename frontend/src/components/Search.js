import React, { Component } from 'react'
import { Form } from 'react-bootstrap'


class Search extends Component {

    filterUpdate() {
        // refers to the ref in the label tag. This has the search keywords to filter by allowing acess to the input.
        const val = this.myValue.value

        //calls parent filterUpdate() passed to props and sends it most recent search keyword(s)
        this.props.filterUpdate(val)

    }

    render() {
        return (
            <header>
                <Form>
                    <Form.Group controlId="formGroupEmail">
                        <Form.Label>
                            Search 
                            {' '}
                            {this.props.model}
                        </Form.Label>
                        <Form.Control 
                            type="text"

                            // pass callback function. myValue is just the name of the ref. Value stores the ref 
                            ref={ (value) => {this.myValue = value}}
    
                            placeholder={this.props.placeHolder}
                            
                            // event listener for changing search query
                            onChange={this.filterUpdate.bind(this)}
                        />
                    </Form.Group>
                </Form>
            </header>
        )
    }






}

export default Search;