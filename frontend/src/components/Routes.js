import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";
import Toolbar from "./Toolbar/Toolbar";
import ScrollToTop from "./ScrollToTop";

import About from "./About/About";
import Search from "./SearchPage/SearchPage";
import Leagues from "./Leagues/Leagues";

import LeagueInstance from "./Leagues/LeagueInstancePage";
import TeamInstance from "./Teams/TeamInstancePage";
import PlayerInstance from "./Players/PlayerInstancePage";
import Visualizations from "./Visualizations/Visualizations";
import Provider from "./Visualizations/Provider";
import Teams from "./Teams/Teams";
import Players from "./Players/Players";
import Home from "./Home/Home";
import history from '../history';

export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Toolbar /> 
                <ScrollToTop />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/About" component={About} />
                    <Route exact path="/Search" component={Search} />
                    <Route exact path="/Leagues" component={Leagues} />
                    <Route exact path="/Visualizations" component={Visualizations} />
                    <Route exact path="/Provider" component={Provider} />
                    <Route path="/Leagues/:path" component={LeagueInstance} />
                    <Route exact path="/Teams" component={Teams} />
                    <Route path="/Teams/:path" component={TeamInstance} />
                    <Route exact path="/Players" component={Players} />
                    <Route path="/Players/:path" component={PlayerInstance} />
                </Switch>
             </Router>
        );
    }
}
