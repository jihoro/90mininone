import React from "react";
import Highlighter from "react-highlight-words";
import "./Highlight.css";

const highlight = (props) =>  {
    const keywords = props.searchQuery.trim().split(' ');
    return <Highlighter
            highlightClassName="highlight"
            searchWords={keywords}
            autoEscape={true}
            textToHighlight={props.word}
        />
    
};

export default highlight;